package com.parkermc.amnesia.events;

import com.parkermc.amnesia.proxy.ProxyCommon;

import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.fml.common.FMLCommonHandler;

@AmnesiaEvent
public class EventMessage implements IAmnesiaEvents{
	
	@Override
	public void random() {
		if(FMLCommonHandler.instance().getMinecraftServerInstance() != null && !ProxyCommon.config.random_message.isEmpty()) {
			FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().sendChatMsg(new TextComponentString(ProxyCommon.config.random_message));
		}
	}

	@Override
	public void normal() {
		if(FMLCommonHandler.instance().getMinecraftServerInstance() != null && !ProxyCommon.config.normal_message.isEmpty()) {
			FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().sendChatMsg(new TextComponentString(ProxyCommon.config.normal_message));
		}
	}

	@Override
	public void updatePost() {
		if(FMLCommonHandler.instance().getMinecraftServerInstance() != null && !ProxyCommon.config.post_change_message.isEmpty()) {
			FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().sendChatMsg(new TextComponentString(ProxyCommon.config.post_change_message));
		}
	}

	@Override
	public void updatePostClient() {
	}

	@Override
	public void cure() {
		if(FMLCommonHandler.instance().getMinecraftServerInstance() != null && !ProxyCommon.config.cure_message.isEmpty()) {
			FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().sendChatMsg(new TextComponentString(ProxyCommon.config.cure_message));
		}
	}
}
