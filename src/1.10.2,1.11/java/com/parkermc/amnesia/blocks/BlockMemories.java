package com.parkermc.amnesia.blocks;

import java.util.List;

import com.parkermc.amnesia.ModMain;
import com.parkermc.amnesia.proxy.ProxyClient;
import com.parkermc.amnesia.proxy.ProxyCommon;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockMemories extends Block {
	public static final String unlocalizedName = "memories";
	
	public BlockMemories() {
		super(Material.ROCK);
		this.setCreativeTab(ModMain.tab);
		this.setUnlocalizedName(unlocalizedName);
		this.setRegistryName(unlocalizedName);
		this.setHardness(3.0F);
		this.setResistance(5.0F);
	}
	
	@SideOnly(Side.CLIENT)
	@Override
	public void addInformation(ItemStack stack, EntityPlayer player, List<String> tooltip, boolean advanced) {
		if(ProxyClient.serverConfig == null) {
			if(!ProxyCommon.config.block_of_memories.hover_text.isEmpty()) {
				tooltip.add(ProxyCommon.config.block_of_memories.hover_text);
			}
		}else {
			if(!ProxyClient.serverConfig.block_of_memories.hover_text.isEmpty()) {
				tooltip.add(ProxyClient.serverConfig.block_of_memories.hover_text);
			}
		}
	}
	
	@SideOnly(Side.CLIENT)
	public void initModel() {
		ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(this), 0, new ModelResourceLocation(this.getRegistryName(), "inventory"));
	}
}
