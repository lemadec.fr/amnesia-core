package com.parkermc.amnesia;

import com.parkermc.amnesia.blocks.*;

import net.minecraft.item.ItemBlock;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

public class ModBlocks {
	public static BlockGoldenSoulSand golden_soul_sand;
	public static BlockMemories memories;
	public static BlockMemoriesCompressed memories_compressed_1;
	public static BlockMemoriesCompressed memories_compressed_2;
	public static BlockMemoriesCompressed memories_compressed_3;
	
	public static void preInit() {
		ForgeRegistries.BLOCKS.register(golden_soul_sand = new BlockGoldenSoulSand());
		ForgeRegistries.ITEMS.register(new ItemBlock(golden_soul_sand).setRegistryName(golden_soul_sand.getRegistryName()));
		ForgeRegistries.BLOCKS.register(memories = new BlockMemories());
		ForgeRegistries.ITEMS.register(new ItemBlock(memories).setRegistryName(memories.getRegistryName()));
		ForgeRegistries.BLOCKS.register(memories_compressed_1 = new BlockMemoriesCompressed(1));
		ForgeRegistries.ITEMS.register(new ItemBlock(memories_compressed_1).setRegistryName(memories_compressed_1.getRegistryName()));
		ForgeRegistries.BLOCKS.register(memories_compressed_2 = new BlockMemoriesCompressed(2));
		ForgeRegistries.ITEMS.register(new ItemBlock(memories_compressed_2).setRegistryName(memories_compressed_2.getRegistryName()));
		ForgeRegistries.BLOCKS.register(memories_compressed_3 = new BlockMemoriesCompressed(3));
		ForgeRegistries.ITEMS.register(new ItemBlock(memories_compressed_3).setRegistryName(memories_compressed_3.getRegistryName()));
	}
	
	public static void initModels() {
		golden_soul_sand.initModel();
		memories.initModel();
		memories_compressed_1.initModel();
		memories_compressed_2.initModel();
		memories_compressed_3.initModel();
	}
	
	public static void updateConfigData() {
	}

}

