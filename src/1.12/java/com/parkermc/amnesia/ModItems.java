package com.parkermc.amnesia;

import com.parkermc.amnesia.items.*;

import net.minecraftforge.fml.common.registry.ForgeRegistries;

public class ModItems {
	public static ItemReset reset;
	public static ItemCure cure;
	public static ItemAmnesiaClock amnesia_clock;
	
	public static void preInit() {
		ForgeRegistries.ITEMS.register(reset = new ItemReset());
		ForgeRegistries.ITEMS.register(cure = new ItemCure());
		ForgeRegistries.ITEMS.register(amnesia_clock = new ItemAmnesiaClock());
	}
	
	public static void initModels() {
		reset.initModel();
		cure.initModel();
		amnesia_clock.initModel();
	}
}
