package com.parkermc.amnesia.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.parkermc.amnesia.events.AmnesiaEventHandler;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.storage.loot.LootTableList;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

public class CommandAmnesia extends CommandBase {

    public String getName(){
        return "amnesia";
    }

    @Override
    public int getRequiredPermissionLevel(){
        return 2;
    }

    public String getUsage(ICommandSender sender){
        return "/amnesia <cure|crafting_classes|loottables|normalise|randomise>";
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException{
        if (args.length != 1){
            throw new WrongUsageException("Wrong amout of arguments", new Object[0]);
        }else{
        	if(args[0].equals("cure")) {
            	AmnesiaEventHandler.cure();
            }else if(args[0].equals("normalise")) {
            	AmnesiaEventHandler.normal();
            }else if(args[0].equals("randomise")) {
            	AmnesiaEventHandler.random();
            }else if(args[0].equals("crafting_classes")) {
            	List<Class<?>> rList = new ArrayList<Class<?>>();
            	for (IRecipe recipe : ForgeRegistries.RECIPES.getValues()) {
            		if(!rList.contains(recipe.getClass())) {
            			rList.add(recipe.getClass());
            		}
            	}
            	String list = "";
            	for(Class<?> recipe : rList) {
            		list += recipe.getName() + "\n";
            	}
            	sender.sendMessage(new TextComponentString(list));
        	}else if(args[0].equals("loottables")) {
            	String list = "";
            	for(ResourceLocation table : LootTableList.getAll()) {
            		list += table.toString() + "\n";
            	}
            	sender.sendMessage(new TextComponentString(list));
            }else {
            	throw new WrongUsageException("Invalid sub command", new Object[0]);
            }
        }
    }

    @Override
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos targetPos){
        return (args.length == 1) ? getListOfStringsMatchingLastWord(args, new String[] {"cure", "crafting_classes",  "loottables", "normalise", "randomise"}) : Collections.<String>emptyList();
    }
}
