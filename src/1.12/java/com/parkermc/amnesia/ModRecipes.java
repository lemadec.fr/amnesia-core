package com.parkermc.amnesia;

import com.parkermc.amnesia.ModMain;

import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ModRecipes {
	
	public static void preInit() {
		GameRegistry.addShapedRecipe(new ResourceLocation(ModMain.MODID, "reset"), null, new ItemStack(ModItems.reset),
			" M ",
			"MBM",
			" M ",
			'M', ModBlocks.memories,
			'B', Blocks.BEACON
		);
		
		GameRegistry.addShapedRecipe(new ResourceLocation(ModMain.MODID, "cure"), null, new ItemStack(ModItems.cure),
			"MMM",
			"MSM",
			"MMM",
			'M', ModBlocks.memories,
			'S', Items.NETHER_STAR
			);
		
		GameRegistry.addShapedRecipe(new ResourceLocation(ModMain.MODID, "amnesia_clock"), null, new ItemStack(ModItems.amnesia_clock), 
				"RrR",
				"rCr",
				"RrR",
				'R', ModItems.cure,
				'r', ModItems.reset,
				'C', Items.CLOCK
				);
		
		GameRegistry.addShapedRecipe(new ResourceLocation(ModMain.MODID, "compressed_memories_1"), null, new ItemStack(ModBlocks.memories_compressed_1), 
				"MMM",
				"MMM",
				"MMM",
				'M', ModBlocks.memories
				);
		
		GameRegistry.addShapedRecipe(new ResourceLocation(ModMain.MODID, "compressed_memories_2"), null, new ItemStack(ModBlocks.memories_compressed_2), 
				"MMM",
				"MMM",
				"MMM",
				'M', ModBlocks.memories_compressed_1
				);
		
		GameRegistry.addShapedRecipe(new ResourceLocation(ModMain.MODID, "compressed_memories_3"), null, new ItemStack(ModBlocks.memories_compressed_3), 
				"MMM",
				"MMM",
				"MMM",
				'M', ModBlocks.memories_compressed_2
				);
		
		
		GameRegistry.addShapelessRecipe(new ResourceLocation(ModMain.MODID, "compressed_memories_1r"), null, new ItemStack(ModBlocks.memories, 9), 
				Ingredient.fromStacks(new ItemStack(ModBlocks.memories_compressed_1)));
		GameRegistry.addShapelessRecipe(new ResourceLocation(ModMain.MODID, "compressed_memories_2r"), null, new ItemStack(ModBlocks.memories_compressed_1, 9), 
				Ingredient.fromStacks(new ItemStack(ModBlocks.memories_compressed_2)));
		GameRegistry.addShapelessRecipe(new ResourceLocation(ModMain.MODID, "compressed_memories_3r"), null, new ItemStack(ModBlocks.memories_compressed_2, 9), 
				Ingredient.fromStacks(new ItemStack(ModBlocks.memories_compressed_3)));
	}
}
