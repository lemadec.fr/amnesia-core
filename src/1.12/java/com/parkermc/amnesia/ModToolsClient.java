package com.parkermc.amnesia;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.entity.player.EntityPlayer;

public class ModToolsClient extends ModTools{
	public final static FontRenderer getFontRenderer(Minecraft mc) {
		return mc.fontRenderer;
	}
	
	public final static WorldClient getWorldClient(Minecraft mc) {
		return mc.world;
	}
	
	public final static EntityPlayer getPlayer(Minecraft mc) {
		return mc.player;
	}
}
