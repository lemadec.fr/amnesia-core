package com.parkermc.amnesia.events;

import com.parkermc.amnesia.data.DataAmnesia;
import com.parkermc.amnesia.network.MessageConfig;
import com.parkermc.amnesia.network.MessageStartTick;
import com.parkermc.amnesia.proxy.ProxyCommon;
import com.parkermc.amnesia.ModMain;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.Phase;

public class EventHandlerWorld {
	
	@SubscribeEvent
	public void onConnect(PlayerLoggedInEvent event) {
		ModMain.network.sendTo(new MessageConfig(ProxyCommon.config), (EntityPlayerMP)event.player);
		ModMain.network.sendTo(new MessageStartTick(DataAmnesia.instance.lastReset), (EntityPlayerMP)event.player);
	}
	
	@SubscribeEvent
	public void onWorldLoad(WorldEvent.Load event) {
		if ( !event.getWorld().isRemote
		  && event.getWorld().provider.getDimension() == 0 ) {
			DataAmnesia.instance = new DataAmnesia();
			DataAmnesia.instance.load(event.getWorld());
		}
	}
	
	@SubscribeEvent
	public void onWorldUnload(WorldEvent.Unload event) {
		if ( !event.getWorld().isRemote
		  && event.getWorld().provider.getDimension() == 0 ) {
			DataAmnesia.instance = null;
		}
	}
	
	@SubscribeEvent
	public void onTick(TickEvent.WorldTickEvent event) {
		if ( ProxyCommon.config.expire_ticks > 0
		  && !event.world.isRemote
		  && event.phase == Phase.START
		  && event.world.provider.getDimension() == 0
		  && event.world.getTotalWorldTime() > ProxyCommon.config.expire_ticks + DataAmnesia.instance.lastReset ) {
			AmnesiaEventHandler.random();
		}
	}
}
