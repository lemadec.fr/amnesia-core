package com.parkermc.amnesia.events;

import net.minecraft.world.World;

import com.parkermc.amnesia.ModMain;
import com.parkermc.amnesia.data.DataAmnesia;
import com.parkermc.amnesia.network.MessageStartTick;
import com.parkermc.amnesia.proxy.ProxyCommon;

import net.minecraftforge.common.DimensionManager;

@AmnesiaEvent
public class EventUpdateTime implements IAmnesiaEvents{
	
	@Override
	public void random() {
	}

	@Override
	public void normal() {
	}

	@Override
	public void cure() {
		World world = DimensionManager.getWorld(0);
		if(!world.isRemote) {
			DataAmnesia.instance.lastReset += ProxyCommon.config.expire_ticks;
			DataAmnesia.instance.save(world);
			ModMain.network.sendToAll(new MessageStartTick(DataAmnesia.instance.lastReset));
		}
	}
	
	@Override
	public void updatePost() {
		World world = DimensionManager.getWorld(0);
		if(!world.isRemote) {
			DataAmnesia.instance.lastReset = world.getTotalWorldTime();
			DataAmnesia.instance.save(world);
			ModMain.network.sendToAll(new MessageStartTick(DataAmnesia.instance.lastReset));
		}
	}

	@Override
	public void updatePostClient() {
	}
}
