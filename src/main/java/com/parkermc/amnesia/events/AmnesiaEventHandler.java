package com.parkermc.amnesia.events;

import java.util.ArrayList;
import java.util.List;

public class AmnesiaEventHandler{
	private static List<IAmnesiaEvents> listeners = new ArrayList<IAmnesiaEvents>();
	
	public static void register(List<IAmnesiaEvents> events) {
		for(IAmnesiaEvents eventHandler : events) {
			register(eventHandler);
		}
	}
	
	public static void register(IAmnesiaEvents event) {
		listeners.add(event);
	}
	
	public static void random() {
		for(IAmnesiaEvents eventHandler : listeners) {
			eventHandler.random();
		}
		AmnesiaEventHandler.updatePost();
	}
	
	public static void cure() {
		for(IAmnesiaEvents eventHandler : listeners) {
			eventHandler.cure();
		}
	}

	public static void normal() {
		for(IAmnesiaEvents eventHandler : listeners) {
			eventHandler.normal();
		}
		AmnesiaEventHandler.updatePost();
	}

	public static void updatePost() {
		for(IAmnesiaEvents eventHandler : listeners) {
			eventHandler.updatePost();
		}
	}
	
	public static void updatePostClient() {
		for(IAmnesiaEvents eventHandler : listeners) {
			eventHandler.updatePostClient();
		}
	}
}
