package com.parkermc.amnesia.blocks;

import com.parkermc.amnesia.ModMain;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockMemoriesCompressed extends Block {
	public final String unlocalizedName = "memories_compressed_";
	
	public BlockMemoriesCompressed(int compressed_val) {
		super(Material.ROCK);
		this.setCreativeTab(ModMain.tab);
		this.setUnlocalizedName(unlocalizedName+compressed_val);
		this.setRegistryName(unlocalizedName+compressed_val);
		this.setHardness(4.0F);
		this.setResistance(5.0F);
	}
	
	@SideOnly(Side.CLIENT)
	public void initModel() {
		ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(this), 0, new ModelResourceLocation(this.getRegistryName(), "inventory"));
	}
}
