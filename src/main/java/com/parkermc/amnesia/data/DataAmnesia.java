package com.parkermc.amnesia.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;

import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class DataAmnesia{
	private static String folder = "data";
	private static String file = "amnesia.dat";
	
	public static DataAmnesia instance;
	
	public long lastReset = 0;
	
	public DataAmnesia(NBTTagCompound nbtTagCompound) {	
		this.readFromNBT(nbtTagCompound);
	}
	
	public DataAmnesia() {
	}
	
	public boolean load(World world) {
		if(world != null) {
			File filename = Paths.get(world.getSaveHandler().getWorldDirectory().getPath(), folder, file).toFile();
			
			if(filename.exists()) {
				try {
	                FileInputStream fileinputstream = new FileInputStream(filename);
	                NBTTagCompound nbttagcompound = CompressedStreamTools.readCompressed(fileinputstream);
	                fileinputstream.close();
	                this.readFromNBT(nbttagcompound.getCompoundTag("data"));
				} catch (IOException e) {
					e.printStackTrace();
				}
				return true;
			}else {
				this.save(world);
				return false;
			}
		}
		return false;
	}
	
	public void readFromNBT(NBTTagCompound nbt) {
		this.lastReset = nbt.getLong("last_reset");
	}
	
	public void save(World world){
		if(world != null) {
			File filename = Paths.get(world.getSaveHandler().getWorldDirectory().getPath(), folder, file).toFile();
			
			try {
				if(!filename.exists()) {
					filename.createNewFile();
				}
				NBTTagCompound nbttagcompound = new NBTTagCompound();
	            nbttagcompound.setTag("data", this.writeToNBT(new NBTTagCompound()));
	            FileOutputStream fileoutputstream = new FileOutputStream(filename);
	            CompressedStreamTools.writeCompressed(nbttagcompound, fileoutputstream);
	            fileoutputstream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public NBTTagCompound writeToNBT(NBTTagCompound compound) {
		compound.setLong("last_reset", this.lastReset);
		return compound;
	}
}
