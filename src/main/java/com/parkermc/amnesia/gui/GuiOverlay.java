package com.parkermc.amnesia.gui;

import com.parkermc.amnesia.ModToolsClient;
import com.parkermc.amnesia.proxy.ProxyClient;
import com.parkermc.amnesia.proxy.ProxyCommon;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.gui.inventory.GuiInventory;
import net.minecraft.client.renderer.GlStateManager;

public class GuiOverlay {
	
	public GuiOverlay(Minecraft mc) {          
        GlStateManager.pushMatrix();
        GlStateManager.scale(1.5, 1.5, 1.5);
        
    	long seconds_left = (ProxyClient.serverConfig.expire_ticks - (ModToolsClient.getWorldClient(mc).getTotalWorldTime() - ProxyClient.amnesia_start_tick)) / 20;
    	String text = String.format("%s%d:%02d", ((seconds_left < 0)?"-":""),  Math.abs(seconds_left) / 60, Math.abs(seconds_left) % 60);
        
        ScaledResolution scaled = new ScaledResolution(mc);
        int width = (int) (scaled.getScaledWidth() / 1.5);
        int height = (int) (scaled.getScaledHeight() / 1.5);
        
        int box_width = ModToolsClient.getFontRenderer(mc).getStringWidth(text) + 5;
        int box_x = getCord(width, box_width, ProxyCommon.config.gui.horizontal);
        int box_y = getCord(height, 13, ProxyCommon.config.gui.vertical);
        
        mc.getTextureManager().bindTexture(GuiInventory.INVENTORY_BACKGROUND);
        mc.ingameGUI.drawTexturedModalRect(box_x, box_y, 5, 169, box_width, 13);
        

        ModToolsClient.getFontRenderer(mc).drawStringWithShadow(text, box_x + 3, box_y + 3, Integer.parseInt("FFFFFF", 16));
        
        GlStateManager.popMatrix();
	}
	
	public static int getCord(int window_cord, int box_cord, int setting) {
        switch (setting) {
		case 0:
			return window_cord / 2 - box_cord / 2;
		case 1:
			return window_cord - box_cord;
		default:
			return 0;
		}
	}
}
