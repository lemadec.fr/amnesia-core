package com.parkermc.amnesia;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.parkermc.amnesia.events.AmnesiaEvent;
import com.parkermc.amnesia.events.IAmnesiaEvents;

import net.minecraftforge.fml.common.discovery.ASMDataTable;

public class AnnotatedUtil {
	
	public static List<IAmnesiaEvents> getEvents(ASMDataTable asmDataTable){
		return getInstances(asmDataTable, AmnesiaEvent.class, IAmnesiaEvents.class);
	}
	
	private static <T> List<T> getInstances(ASMDataTable asmDataTable, Class<?> annotationClass, Class<T> instanceClass) {
		String annotationClassName = annotationClass.getCanonicalName();
		Set<ASMDataTable.ASMData> asmDatas = asmDataTable.getAll(annotationClassName);
		List<T> instances = new ArrayList<>();
		for (ASMDataTable.ASMData asmData : asmDatas) {
			try {
				Class<?> asmClass = Class.forName(asmData.getClassName());
				Class<? extends T> asmInstanceClass = asmClass.asSubclass(instanceClass);
				T instance = asmInstanceClass.newInstance();
				instances.add(instance);
			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | LinkageError e) {
			}
		}
		return instances;
	}
}
