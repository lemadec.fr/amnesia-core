package com.parkermc.amnesia.proxy;

import java.io.File;
import java.io.IOException;

import com.parkermc.amnesia.AnnotatedUtil;
import com.parkermc.amnesia.Config;
import com.parkermc.amnesia.ModBlocks;
import com.parkermc.amnesia.ModItems;
import com.parkermc.amnesia.ModMain;
import com.parkermc.amnesia.ModRecipes;
import com.parkermc.amnesia.ModTools;
import com.parkermc.amnesia.events.AmnesiaEventHandler;
import com.parkermc.amnesia.events.EventHandlerWorld;
import com.parkermc.amnesia.events.EventLootTable;
import com.parkermc.amnesia.worldgen.OreGen;
import org.apache.logging.log4j.Logger;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ProxyCommon {
	public static Config config = new Config();
	public static File configFile;
	private EventLootTable eventLootTable = new EventLootTable();
	private EventHandlerWorld eventHandlerWorldLoad = new EventHandlerWorld();
	public static Logger logger;
	
	public void preInit(FMLPreInitializationEvent event) {
		logger = event.getModLog();
		MinecraftForge.EVENT_BUS.register(eventLootTable);
		MinecraftForge.EVENT_BUS.register(eventHandlerWorldLoad);
		configFile = new File(event.getModConfigurationDirectory(), ModMain.MODID+".json");
		try {
			config = Config.Read(configFile);
		} catch (IOException e) {
			ModTools.logError("Error loading config resetting to defaults");
			e.printStackTrace();
		}
		ModItems.preInit();
		ModBlocks.preInit();
		ModRecipes.preInit();
		GameRegistry.registerWorldGenerator(new OreGen(), 3);
        AmnesiaEventHandler.register(AnnotatedUtil.getEvents(event.getAsmData()));
	}
	
	public void init(FMLInitializationEvent event) {
	}
	
	public void postInit(FMLPostInitializationEvent event) {
	}
}
