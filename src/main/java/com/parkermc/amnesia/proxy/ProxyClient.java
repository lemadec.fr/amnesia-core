package com.parkermc.amnesia.proxy;

import com.parkermc.amnesia.Config;
import com.parkermc.amnesia.ModBlocks;
import com.parkermc.amnesia.ModItems;
import com.parkermc.amnesia.events.EventHandlerClient;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

public class ProxyClient extends ProxyCommon {
	private EventHandlerClient eventHandlerConnect = new EventHandlerClient();
	
	public static Config serverConfig = null;
	public static long amnesia_start_tick = -1;

	public void preInit(FMLPreInitializationEvent event) {
		super.preInit(event);
		MinecraftForge.EVENT_BUS.register(eventHandlerConnect);
		ModItems.initModels();
		ModBlocks.initModels();
	}
	
	public void init(FMLInitializationEvent event) {
		super.init(event);
	}
	
	public void postInit(FMLPostInitializationEvent event) {
		super.postInit(event);
	}

}
