package com.parkermc.amnesia;

import org.apache.logging.log4j.Level;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.FMLLog;

public class ModTools {
	
	public final static ItemStack getItemStack(NBTTagCompound tag){
		return new ItemStack(tag);
	}
	
	public final static void logError(String format, Object... data) {
		FMLLog.log(Level.ERROR, format, data);
	}
	
	public final static void sendMessageAll(ITextComponent msg) {
		FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().sendMessage(msg);
	}
	
	public final static World getWorld(EntityPlayer player) {
		return player.world;
	}
}
